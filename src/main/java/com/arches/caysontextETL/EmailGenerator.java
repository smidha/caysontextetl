package com.arches.caysontextETL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Locale.Category;
import java.util.logging.Level;

import org.apache.commons.lang3.ArrayUtils;

import com.google.gson.Gson;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.microtripit.mandrillapp.lutung.view.MandrillUserInfo;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;

public class EmailGenerator {

public static String summaryEmailOn=null;
public static String apiKey=null;
public static String senderName=null;
public static String senderEmail=null;
public static String[] recipientsNames=null;
public static String[] recipientsEmails=null;
	

	public EmailGenerator()
	{
		ResourceBundle mandrillBundle=ResourceBundle.getBundle("MandrillProperties");
		summaryEmailOn=mandrillBundle.getString("summaryEmailOn");
		apiKey=mandrillBundle.getString("apiKey");
		senderEmail=mandrillBundle.getString("senderEmail");
		senderName=mandrillBundle.getString("senderName");
		if(mandrillBundle.getString("recipientsEmails")==null)
		{
			recipientsEmails=new String[0];
		}
		else
		{
		recipientsEmails=(mandrillBundle.getString("recipientsEmails")).split(",");
		}
		
		if(mandrillBundle.getString("recipientsEmails")==null)
		{		
				recipientsNames=new String[0];
		}
		else
		{
			recipientsNames=mandrillBundle.getString("recipientsNames").split(",");
		}
	}

	public void generateSummaryEmail(long total,long smsSent)
	{
		if(summaryEmailOn==null || summaryEmailOn.equals("false"))
    	{
    		return;
    	}
    
		//MandrillConfiguration gets the mandrill account properties
        MandrillApi mandrillApi=new MandrillApi(this.apiKey);
        MandrillUserInfo user;
		try 
		{
			user = mandrillApi.users().info();
		} 
		catch (MandrillApiError e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			App.caystonTextETLLogger.log(Level.SEVERE,e1.getMessage(),e1);
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
			App.caystonTextETLLogger.log(Level.SEVERE,e1.getMessage(),e1);
			
		}
     
        

       //build email message and add sender information
        MandrillMessage message=new MandrillMessage();
        message.setSubject("Cayston Text Generation Summary For :"+Calendar.getInstance().getTime());
        String styleTemplate="<head><style>"
        		+"h4{color:151C5C;"
        		+ "font-family:Tohoma;}" 
        		+ "#entries {font-family: Tohoma;border-collapse: collapse;width: 100%;font-size:80%;}"
        +"#entries td, #entries th {"
         +"border: 1px solid #ddd;"
         +"text-align: left;"
         +"padding: 8px;}"
         +"#entries tr:nth-child(even){background-color: #f2f2f2}"
         +"#entries tr:hover {background-color: #ddd;}"
         +"#entries th {"
         +"padding-top: 12px;"
         +"padding-bottom: 12px;"
         +"background-color: #0D197C;"
         +"color: white;"
         + "font-weight:bold}"
         +"</style></head>";
        message.setHtml(styleTemplate+"<body><h4 align='center'>Summary for Cayston Text Messages Sent After DB Scan On  "+Calendar.getInstance().getTime()+"</h4>"
         +"<p><table id=\"entries\" border=\"1px\"><tr><b><th>Title</th><th>Count</th></b></tr>"
        		+ "<tr><td>Total users fetched from DB</td><td>"+total+"</td></tr><br>"
        		+ "<tr><td>Total SMS sent </td><td>"+smsSent+"</td></tr><br>"
        		+ "</p></body>");
       
         message.setAutoText(true);
        message.setFromEmail(this.senderEmail);
        message.setFromName(this.senderName);

         List<Recipient> recipients = new ArrayList<Recipient>();  
        for(int i=0;i<recipientsEmails.length;i++)
        {       
        	Recipient recipient = new Recipient();
        	recipient.setEmail(recipientsEmails[i]);
        	recipient.setName(recipientsNames[i]);
        		recipients.add(recipient);
        }
        message.setTo(recipients);
        message.setPreserveRecipients(true);
        try {
      //      send the email
      
          MandrillMessageStatus[] messageStatusReports = mandrillApi.messages().send(message, false);
          for(int j=0;j<messageStatusReports.length;j++)
          {
            System.out.println("Summary Email Sent Status:"+messageStatusReports[j].getStatus());
          } 
        } 
        catch (MandrillApiError ex)
        {
        System.out.println("mandrill exception."+ex);
      
        App.caystonTextETLLogger.log(Level.SEVERE,ex.getMessage(),ex);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	App.caystonTextETLLogger.log(Level.SEVERE,e.getMessage(),e);
        }
    }

		
	}
	

