package com.arches.caysontextETL;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TextEligibleUsers 
{
	public Map<String,String> getTextEligibleUsers(String firstRunFlag)
	{
				Map<String,String> phoneNumbersMap=new HashMap<String, String>();
				List<Object> phoneNumbersQueryResult=null;
				Configuration cfg=null;
				SessionFactory sessionFactory=null;
				Session session=null;
				
				try
				{
					
					System.out.println("getting text eligible users..");
					cfg=new Configuration();
					cfg.configure(App.hibernatePath);
					sessionFactory=cfg.buildSessionFactory();
					session=sessionFactory.openSession();
			
					
					//OLD QUERY: String hql_textEligibleUsers="select UD.uuid,UD.contactNumber from UserDet UD where  (UD.zip is not null and UD.zip!='')  and (UD.address1 is not null and UD.address1!='')  and (UD.email is null or UD.email='') and (UD.contactNumber is not null and UD.contactNumber!='') and UD.isCrmEnrolled=0 and (:currentTS-createdOn<=24*60*60*1000)";
					String hql_textEligibleUsers="select UD.uuid,UD.contactNumber from UserDet UD where  (UD.contactNumber is not null and UD.contactNumber!='') and UD.isCrmEnrolled=0 and (:currentTS-createdOn<=24*60*60*1000)";
					String firstRunQuery="select UD.uuid,UD.contactNumber from UserDet UD where  (UD.zip is not null and UD.zip!='')  and (UD.address1 is not null and UD.address1!='')  and (UD.email is null or UD.email='') and (UD.contactNumber is not null and UD.contactNumber!='')  and UD.isCrmEnrolled=0";
					String selectedQuery=null;
					Query query_phoneNumbers= null;
					if(firstRunFlag.trim().equals("true"))
					{
						System.out.println("first run...");
						selectedQuery=firstRunQuery;
						query_phoneNumbers=session.createQuery(selectedQuery);
					}
					else
					{
						System.out.println("not the first run..");
						selectedQuery=hql_textEligibleUsers;
						query_phoneNumbers=session.createQuery(selectedQuery);
						query_phoneNumbers.setParameter("currentTS",new BigDecimal(System.currentTimeMillis()));
						
					}
					phoneNumbersQueryResult=query_phoneNumbers.list();
					for (Iterator iterator = phoneNumbersQueryResult.iterator(); iterator.hasNext();)
					{
						Object[] row= (Object[]) iterator.next();
						phoneNumbersMap.put(row[0].toString(),row[1].toString());
					}
					
				}
				
				catch(Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					if(session!=null)
					{
						session.flush();
						session.close();
						
					}
				}
			
				return  phoneNumbersMap;
	}	
	
	
}
