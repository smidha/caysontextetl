package com.arches.caysontextETL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;

import com.arches.utilities.Bitlytest;
import com.textmagic.sdk.RestClient;
import com.textmagic.sdk.RestException;
import com.textmagic.sdk.resource.instance.TMNewMessage;

public class TextGenerator 
{
	String textMessageFile;
	String text;
	String bitlyUsername;
	String bitlyPassword;
	String reprofileURL;
	String textMagicUsername;
	String textMagicPassword;
	String textServiceFlag;
	
	
	public TextGenerator(String textMessageFile)
	{
		this.textMessageFile=textMessageFile;
		ResourceBundle rb=ResourceBundle.getBundle(this.textMessageFile);
		this.bitlyUsername=rb.getString("bitlyUsername").trim();
		this.bitlyPassword=rb.getString("bitlyPassword").trim();
		this.reprofileURL=rb.getString("reprofileURL");
		this.text=rb.getString("text");
		this.textMagicUsername=rb.getString("textMagicUsername").trim();
		this.textMagicPassword=rb.getString("textMagicPassword").trim();
		this.textServiceFlag=rb.getString("textServiceFlag");
	

	}

	
	
	
public void generateWelcomeText(String userUUID,String phone)
{
	
	
	
	String shortURL=null;
	String tempreprofileURL=this.reprofileURL;
	String tempText=this.text;
	
	try 
	{
		if(textServiceFlag.equals("false"))
		{
			
			return;
		}
		
		
		
		tempreprofileURL=tempreprofileURL.replace("[userUUID]",userUUID);
	
		 shortURL=new Bitlytest(this.bitlyUsername,this.bitlyPassword).getShortUrl(tempreprofileURL);
		 System.out.println("user:"+userUUID+" short url is:"+shortURL+" long url:"+tempreprofileURL);
	} 
	catch (Exception e) {
	
			App.caystonTextETLLogger.log(Level.SEVERE,e.getMessage(),e);
		e.printStackTrace();
	}
		
		tempText=tempText.replace("[shortURL]",shortURL);
		System.out.println("text body:"+tempText);
		
	RestClient client = new RestClient(this.textMagicUsername,this.textMagicPassword);
		TMNewMessage m = client.getResource(TMNewMessage.class);
	
		
	 m.setText(tempText);
	    
	    m.setPhones(Arrays.asList(new String[]{"+1"+phone}));
	    try 
	    {
	    	m.send();
	    	 App.smsSent++;
	    }
	    catch (final RestException e) 
	    {
	    e.printStackTrace();
	     System.out.println(e.getErrors());
	      
	     App.caystonTextETLLogger.log(Level.SEVERE,e.getMessage(),e);
	   }
	   
	    System.out.println("text sent:"+m.getId());
	   
}
	
}
