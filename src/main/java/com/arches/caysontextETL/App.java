package com.arches.caysontextETL;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import com.arches.utilities.DateTime;

/**
 * Hello world!
 *
 */
public class App 
{
	public static Logger caystonTextETLLogger=null;
	public static String hibernatePath;
	public static String textMessageProperties=null;
	public static String caystonTextETLLogsLocation=null;
	public static String firstRunFlag=null;
	public static int smsSent=0;
	public static int totalUsers=0;
	
    public static void main( String[] args )
    {
    	try
    	{
    		configureApp();
    		
	        TextEligibleUsers textEligibleUsers=new TextEligibleUsers();
	        //uuid and phone nos map
	        Map<String,String> phoneNumbersMap=textEligibleUsers.getTextEligibleUsers(App.firstRunFlag);
	        
	        //prepare list of nos. to be skipped
	        List<String> skipNosList=new ArrayList<String>();
	        skipNosList.add("7708416390");
	        
	        
	        TextGenerator textGenerator=new TextGenerator(App.textMessageProperties);
	        System.out.println("Iterating over map of size:"+phoneNumbersMap.size()+" to generate text");
	        
	        App.totalUsers=phoneNumbersMap.size();
	        
	        caystonTextETLLogger.log(Level.INFO,"Iterating over map of size:"+phoneNumbersMap.size()+" from DB to generate text");
	        
	        int sleepTimer=0; //for bitly
	        for (Map.Entry<String,String> entry : phoneNumbersMap.entrySet())
	        {
	        	
	        	if(skipNosList.contains(entry.getValue()))
        		{
        			//move to the next record and do not send sms to this record
        			continue;
        		}
        		
	        	sleepTimer++; //increment for each phone no.(i.e. user)
	        	
	        	textGenerator.generateWelcomeText(entry.getKey(),entry.getValue());
	            
	            if(sleepTimer%90==0)
	            {
	            	//sleep for 65 seconds after every 90 records
	            	Thread.sleep(65*1000);
	            }
	        }
	        caystonTextETLLogger.log(Level.INFO,"Count of SMS sent: "+smsSent);
	        EmailGenerator emailGenerator=new EmailGenerator();
	        emailGenerator.generateSummaryEmail(totalUsers, smsSent);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		
			for(Handler h:App.caystonTextETLLogger.getHandlers())
			{
			    h.close();   //must call h.close or a .LCK file will remain.
			}
			
			System.exit(0);

    	}
    }
    	public static void configureApp()
    	{
    		
    		ResourceBundle configurationBundle=ResourceBundle.getBundle("config");
    		App.hibernatePath=configurationBundle.getString("hibernatePath");
    		App.caystonTextETLLogsLocation=configurationBundle.getString("caystonTextETLLogsLocation");
    		App.textMessageProperties=configurationBundle.getString("textMessagePropertiesFile");
    		App.firstRunFlag=configurationBundle.getString("firstRunFlag");
    		
    		caystonTextETLLogger=Logger.getLogger("caystonTextETLLogger");
        	
    		FileHandler fileHandler;
    		try {
    	  		fileHandler=new FileHandler(caystonTextETLLogsLocation+DateTime.getDateTime()+".txt",true);
    	  		caystonTextETLLogger.addHandler(fileHandler);
    	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
    	  		fileHandler.setFormatter(simpleFormatter);
    	  	   caystonTextETLLogger.log(Level.INFO,"Logging for date:"+DateTime.getDateTime(),"Logging for date:"+DateTime.getDateTime());
    	  	}
    	  	catch(Exception e)
    	  	{
    	  		System.out.println("Exception in configuring logger."+e);
    	  		System.exit(1);
    	  	}

    	}
}
