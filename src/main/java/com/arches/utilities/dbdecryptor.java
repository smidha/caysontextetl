package com.arches.utilities;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.salt.FixedStringSaltGenerator;
import org.jasypt.salt.SaltGenerator;
import org.jasypt.salt.ZeroSaltGenerator;
import org.jasypt.util.digest.Digester;

public class dbdecryptor {
	public String decryptText(String text)
    {
        DBConfiguration dbc=new DBConfiguration();
        dbc.configureDB("com.arches.resources.DBConfiguration");

        SaltGenerator salt = new FixedStringSaltGenerator();
        salt = new ZeroSaltGenerator();

        Digester digester = new Digester();
        digester.setAlgorithm(DBConfiguration.digestorAlgorithm);
        
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        encryptor.setPassword(DBConfiguration.encryptionPassword);
        
        encryptor.setSaltGenerator(salt);
        encryptor.setPoolSize(2);
        encryptor.setAlgorithm(DBConfiguration.encryptionAlgorithm);

        String encText = encryptor.decrypt(text);
        return encText;
     }
	public static void main(String[] args)
	{
		dbdecryptor dbd=new dbdecryptor();
		System.out.println(dbd.decryptText("K7/WLrkLpmmdBpQmpiJWnpBTESSP2Id0"));
		
	}
}
